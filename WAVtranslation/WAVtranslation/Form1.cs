﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;



namespace WAVtranslation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        private string filepath = ""; //путь к файлу

        static UDP clientUDP;
        static FpsCommunication fps;

        private int myport;   //порт этого устройства
        private int fpsport;  //порт ФПС
        private string myIP;  //IP этого устройства
        private string fpsIP; //IP ФПС

        private byte addressSender;
        private byte addressRecipient;

        private int frequency;
        

        public void Form1_Load(object sender, EventArgs e)
        {
            //btnStartOnline.Enabled = false;
            //btnTranslateRecord.Enabled = false;
            //btnStop.Enabled =false;
            //btnStopOnline.Enabled = false;
        }


        //для проверки
        static void SaySomething(byte bCode, object obj)
        {
            switch (bCode)
            {
                case 0:
                    MessageBox.Show("Я получил кодограмму");
                    break;


                case 4:
                    MessageBox.Show("Enter Start audio thread OK");
                    break;


                case 5:
                    MessageBox.Show("Start audio thread OK");
                    break;


                case 6:
                    MessageBox.Show("Data for send is available OK");
                    break;

                case 7:
                      MessageBox.Show((string)(obj));
                    // MessageBox.Show("Audio doesn't work");
                    break;

                default:
                    break;
            }
            
            Console.WriteLine("The threshold was reached.");
        }
        static void Warning()
        {
            MessageBox.Show("ФПС не отвечает");
        }

        private void btnOpenFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog(); //Declare the File Dialog
            ofd.Filter = "WAVE Audio File (.wav)|*.wav";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                filepath = ofd.FileName;   //указываем путь к файлу
                textFolder.Text = ofd.FileName;
            }
        }

        //отправка 17 кодограмм с аудиозаписью из файла (в цикле)
        private void btnTranslateRecord_Click(object sender, EventArgs e)
        {
            try
            {

                fps.SendConfirmation(1);
                fps.StartSendVoice(filepath);
                //btnTranslateRecord.Enabled = false;
            }
            catch
            {
                MessageBox.Show("Что-то пошло не так. Проверьте путь к файлу");
            }

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            fps.StopTranslation();
            //btnTranslateRecord.Enabled = true; //исправить, вызывать событие
            fps.SendConfirmation(0);
            //btnConnect.Enabled = true;
        }

        //отправка 16 кодограммы с режимом 1
        private void btnConnect_Click(object sender, EventArgs e)
        {

            //btnStop.Enabled = false;
            //btnTranslateRecord.Enabled = false;
            myport = Convert.ToInt32(textMyPort.Text);
            fpsport = Convert.ToInt32(textFpsPort.Text);
            myIP = textMyIP.Text;
            fpsIP = textFpsIP.Text;

            addressSender = Convert.ToByte(textSenderAddress.Text);
            addressRecipient = Convert.ToByte(textRecipientAddress.Text);

            frequency = Convert.ToInt32(textFrequency.Text);


            clientUDP = new UDP(myport, myIP, fpsport, fpsIP);
            fps = new FpsCommunication(clientUDP, 3, frequency, addressSender, addressRecipient);
      
            clientUDP.Connect();


           // fps.ReceiveCmd += SaySomething; //для проверки принятия кодограмм будет выводить сообщение
            

            
            fps.LostFPS += Warning;
            //btnConnect.Enabled = false;
            //btnTranslateRecord.Enabled = true;
            //btnStop.Enabled = true;
            //btnStartOnline.Enabled = true;
            //btnStopOnline.Enabled = true;

        }


        //отправка 16 кодограммы с режимом 0
        private void btnDisconnect_Click(object sender, EventArgs e)
        {

            //btnConnect.Enabled = true;
            //btnStop.Enabled = false;
            //btnTranslateRecord.Enabled = false;
            //btnStartOnline.Enabled = false;
            //btnStopOnline.Enabled = false;

            //if (UDP.socket != null)
            //{
            //    //UDP.socket.Dispose();
            //    UDP.socket.Close();
            //}
        }

        private void btnStartOnline_Click(object sender, EventArgs e)
        {
            fps.SendConfirmation(1);
            int samleRate = Convert.ToInt32(textSampleRate.Text);
           int bitsInSamle = Convert.ToInt32(textBitsInSamle.Text);
            fps.numberOfDevice = Convert.ToInt32(numericDevice.Value);
            fps.bufferSize = Convert.ToInt32(textBufferSize.Text);
            try
            {
                fps.StartSendVoice(samleRate, bitsInSamle);
            }
            catch
            {
                Console.WriteLine("Онлайн не работает");
            }
        }

        private void btnStopOnline_Click(object sender, EventArgs e)
        {
            fps.StopOnline();
            fps.SendConfirmation(0);
        }
    }
}
