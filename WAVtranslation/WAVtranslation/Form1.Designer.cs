﻿namespace WAVtranslation
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textFolder = new System.Windows.Forms.TextBox();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.btnTranslateRecord = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textMyPort = new System.Windows.Forms.TextBox();
            this.textFpsIP = new System.Windows.Forms.TextBox();
            this.textFpsPort = new System.Windows.Forms.TextBox();
            this.btnConnect = new System.Windows.Forms.Button();
            this.btnDisconnect = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textMyIP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnStartOnline = new System.Windows.Forms.Button();
            this.btnStopOnline = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textRecipientAddress = new System.Windows.Forms.TextBox();
            this.textSenderAddress = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.textFrequency = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textSampleRate = new System.Windows.Forms.TextBox();
            this.textBitsInSamle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.numericDevice = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.textBufferSize = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericDevice)).BeginInit();
            this.SuspendLayout();
            // 
            // textFolder
            // 
            this.textFolder.Location = new System.Drawing.Point(19, 247);
            this.textFolder.Name = "textFolder";
            this.textFolder.Size = new System.Drawing.Size(210, 20);
            this.textFolder.TabIndex = 0;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(236, 244);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(85, 23);
            this.btnOpenFile.TabIndex = 1;
            this.btnOpenFile.Text = "Open file";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            this.btnOpenFile.Click += new System.EventHandler(this.btnOpenFile_Click);
            // 
            // btnTranslateRecord
            // 
            this.btnTranslateRecord.Location = new System.Drawing.Point(19, 273);
            this.btnTranslateRecord.Name = "btnTranslateRecord";
            this.btnTranslateRecord.Size = new System.Drawing.Size(192, 43);
            this.btnTranslateRecord.TabIndex = 2;
            this.btnTranslateRecord.Text = "Translate record";
            this.btnTranslateRecord.UseVisualStyleBackColor = true;
            this.btnTranslateRecord.Click += new System.EventHandler(this.btnTranslateRecord_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(217, 273);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(103, 43);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Local:  IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(46, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "IP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(37, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Port";
            // 
            // textMyPort
            // 
            this.textMyPort.Location = new System.Drawing.Point(199, 10);
            this.textMyPort.Name = "textMyPort";
            this.textMyPort.Size = new System.Drawing.Size(42, 20);
            this.textMyPort.TabIndex = 8;
            this.textMyPort.Text = "9108";
            // 
            // textFpsIP
            // 
            this.textFpsIP.Location = new System.Drawing.Point(69, 151);
            this.textFpsIP.Name = "textFpsIP";
            this.textFpsIP.Size = new System.Drawing.Size(95, 20);
            this.textFpsIP.TabIndex = 9;
            this.textFpsIP.Text = "192.168.0.114";
            // 
            // textFpsPort
            // 
            this.textFpsPort.Location = new System.Drawing.Point(69, 176);
            this.textFpsPort.Name = "textFpsPort";
            this.textFpsPort.Size = new System.Drawing.Size(42, 20);
            this.textFpsPort.TabIndex = 10;
            this.textFpsPort.Text = "9114";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(199, 140);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(103, 23);
            this.btnConnect.TabIndex = 11;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnDisconnect
            // 
            this.btnDisconnect.Location = new System.Drawing.Point(199, 169);
            this.btnDisconnect.Name = "btnDisconnect";
            this.btnDisconnect.Size = new System.Drawing.Size(103, 23);
            this.btnDisconnect.TabIndex = 12;
            this.btnDisconnect.Text = "Disconnect";
            this.btnDisconnect.UseVisualStyleBackColor = true;
            this.btnDisconnect.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 135);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "FPS:";
            // 
            // textMyIP
            // 
            this.textMyIP.Location = new System.Drawing.Point(66, 10);
            this.textMyIP.Name = "textMyIP";
            this.textMyIP.Size = new System.Drawing.Size(95, 20);
            this.textMyIP.TabIndex = 14;
            this.textMyIP.Text = "0.0.0.0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(167, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Port";
            // 
            // btnStartOnline
            // 
            this.btnStartOnline.Location = new System.Drawing.Point(20, 420);
            this.btnStartOnline.Name = "btnStartOnline";
            this.btnStartOnline.Size = new System.Drawing.Size(161, 43);
            this.btnStartOnline.TabIndex = 16;
            this.btnStartOnline.Text = "Online translation";
            this.btnStartOnline.UseVisualStyleBackColor = true;
            this.btnStartOnline.Click += new System.EventHandler(this.btnStartOnline_Click);
            // 
            // btnStopOnline
            // 
            this.btnStopOnline.Location = new System.Drawing.Point(187, 420);
            this.btnStopOnline.Name = "btnStopOnline";
            this.btnStopOnline.Size = new System.Drawing.Size(133, 43);
            this.btnStopOnline.TabIndex = 17;
            this.btnStopOnline.Text = "Stop translation";
            this.btnStopOnline.UseVisualStyleBackColor = true;
            this.btnStopOnline.Click += new System.EventHandler(this.btnStopOnline_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(113, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "RECORD TRANSLATION";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(113, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(126, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "ONLINE TRANSLATION";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(-23, 205);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(511, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "_________________________________________________________________________________" +
    "___";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-96, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(511, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "_________________________________________________________________________________" +
    "___";
            // 
            // textRecipientAddress
            // 
            this.textRecipientAddress.Location = new System.Drawing.Point(116, 88);
            this.textRecipientAddress.Name = "textRecipientAddress";
            this.textRecipientAddress.Size = new System.Drawing.Size(23, 20);
            this.textRecipientAddress.TabIndex = 22;
            this.textRecipientAddress.Text = "4";
            // 
            // textSenderAddress
            // 
            this.textSenderAddress.Location = new System.Drawing.Point(116, 62);
            this.textSenderAddress.Name = "textSenderAddress";
            this.textSenderAddress.Size = new System.Drawing.Size(23, 20);
            this.textSenderAddress.TabIndex = 23;
            this.textSenderAddress.Text = "2";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(110, 46);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Address:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(69, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Sender";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(60, 91);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 26;
            this.label12.Text = "Recipient";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(196, 62);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "Frequency:";
            // 
            // textFrequency
            // 
            this.textFrequency.Location = new System.Drawing.Point(199, 78);
            this.textFrequency.Name = "textFrequency";
            this.textFrequency.Size = new System.Drawing.Size(55, 20);
            this.textFrequency.TabIndex = 28;
            this.textFrequency.Text = "96500";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(17, 362);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(66, 13);
            this.label14.TabIndex = 29;
            this.label14.Text = "Sample rate:";
            // 
            // textSampleRate
            // 
            this.textSampleRate.Location = new System.Drawing.Point(89, 359);
            this.textSampleRate.Name = "textSampleRate";
            this.textSampleRate.Size = new System.Drawing.Size(42, 20);
            this.textSampleRate.TabIndex = 30;
            this.textSampleRate.Text = "8000";
            // 
            // textBitsInSamle
            // 
            this.textBitsInSamle.Location = new System.Drawing.Point(289, 359);
            this.textBitsInSamle.Name = "textBitsInSamle";
            this.textBitsInSamle.Size = new System.Drawing.Size(23, 20);
            this.textBitsInSamle.TabIndex = 32;
            this.textBitsInSamle.Text = "8";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(164, 362);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 13);
            this.label15.TabIndex = 31;
            this.label15.Text = "Number of bits in samle:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 392);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(82, 13);
            this.label16.TabIndex = 33;
            this.label16.Text = "Device number:";
            // 
            // numericDevice
            // 
            this.numericDevice.Location = new System.Drawing.Point(116, 392);
            this.numericDevice.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericDevice.Name = "numericDevice";
            this.numericDevice.Size = new System.Drawing.Size(42, 20);
            this.numericDevice.TabIndex = 34;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(257, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(26, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "kHz";
            // 
            // textBufferSize
            // 
            this.textBufferSize.Location = new System.Drawing.Point(260, 389);
            this.textBufferSize.Name = "textBufferSize";
            this.textBufferSize.Size = new System.Drawing.Size(34, 20);
            this.textBufferSize.TabIndex = 36;
            this.textBufferSize.Text = "128";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(195, 394);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 37;
            this.label18.Text = "Buffer size:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(292, 392);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(20, 13);
            this.label19.TabIndex = 38;
            this.label19.Text = "ms";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(332, 475);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.textBufferSize);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.numericDevice);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBitsInSamle);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textSampleRate);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textFrequency);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textSenderAddress);
            this.Controls.Add(this.textRecipientAddress);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnStopOnline);
            this.Controls.Add(this.btnStartOnline);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textMyIP);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnDisconnect);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.textFpsPort);
            this.Controls.Add(this.textFpsIP);
            this.Controls.Add(this.textMyPort);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnTranslateRecord);
            this.Controls.Add(this.btnOpenFile);
            this.Controls.Add(this.textFolder);
            this.Name = "Form1";
            this.Text = "WAV translation";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericDevice)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textFolder;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button btnTranslateRecord;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textMyPort;
        private System.Windows.Forms.TextBox textFpsIP;
        private System.Windows.Forms.TextBox textFpsPort;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Button btnDisconnect;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textMyIP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnStartOnline;
        private System.Windows.Forms.Button btnStopOnline;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textRecipientAddress;
        private System.Windows.Forms.TextBox textSenderAddress;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textFrequency;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textSampleRate;
        private System.Windows.Forms.TextBox textBitsInSamle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown numericDevice;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBufferSize;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
    }
}

