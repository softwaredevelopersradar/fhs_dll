﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAVtranslation
{
    class DecodingAllTypesError
    {
        TranscriptOfErrors UnpositionalError;
        TranscriptPositionError PositionalError;
        int iAmountOfBytesForDecoding;
        public DecodingAllTypesError(int AmountOfBytesForDecoding)
        {
            iAmountOfBytesForDecoding = AmountOfBytesForDecoding;//првить, создавать только один объект
            UnpositionalError = new TranscriptOfErrors();
            PositionalError = new TranscriptPositionError();
        }
        public String DecodingError(byte[] Error)
        {
            if (Error.Length == 2)
                return UnpositionalError.DecodingError(Error);
            else if (Error.Length == 3)
                return PositionalError.TranscriptionOfError(Error);
            else
                return null;
        }
        public bool IsNotThereError(byte[] Error)
        {
            if (Error.Length == 2)
                return UnpositionalError.IsNotThereError(Error);
            else if (Error.Length == 3)
                return PositionalError.IsNotThereError(Error);
            else
                return false;
        }
    }
}
