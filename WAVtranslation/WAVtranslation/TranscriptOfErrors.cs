﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace WAVtranslation
{
    class TranscriptOfErrors
    {
        const byte DURAT_PARAM_FWS = 5;
        const byte PARAM_FHSS = 11;
        static String[] sForCombination = new String[] { "1/3", "2/4", "5", "6", "7" };//пока случай для 7
        Dictionary<byte, string> DictionaryOfCommandError;
        Dictionary<byte, string> DictionaryOfUniqueErrorForDURAT_PARAM_FWS;
        Dictionary<byte, string> DictionaryOfUniqueErrorForPARAM_FHSS;
        //
        public TranscriptOfErrors()
        {
            DictionaryOfCommandError = new Dictionary<byte, string>();
            DictionaryOfCommandError.Add(0, "верно");
            DictionaryOfCommandError.Add(1, "программная неопределенная");
            DictionaryOfCommandError.Add(20, "аппаратная неопределенная");
            //
            DictionaryOfCommandError.Add(7, "превышено кол-во частот в литере 6");
            DictionaryOfCommandError.Add(8, "превышено кол-во частот в литере 7");
            DictionaryOfCommandError.Add(9, "значение частоты вне диапазона");
            DictionaryOfCommandError.Add(10, "значение длительности вне диапазона");
            DictionaryOfCommandError.Add(11, "неверный код модуляции ");
            DictionaryOfCommandError.Add(12, "неверный код девиации");
            DictionaryOfCommandError.Add(13, "неверный код манипуляции");
            //
            for (int i1 = 33; i1 <= 63; i1++)
            {
                String ForNoAnswerFromSynthesizerShaperLetters = "нет ответа от синтезатора формирователя литеры";
                byte[] iNumberForDescription = new byte[] { Convert.ToByte(i1 - 32) };
                BitArray btForFormula = new BitArray(iNumberForDescription);
                for (int i2 = 0; i2 < 5; i2++)
                    if (btForFormula[i2] == true)
                        ForNoAnswerFromSynthesizerShaperLetters = String.Concat(ForNoAnswerFromSynthesizerShaperLetters, " ", sForCombination[i2]);
                DictionaryOfCommandError.Add(Convert.ToByte(i1), ForNoAnswerFromSynthesizerShaperLetters);
            }
            for (int i1 = 65; i1 <= 95; i1++)
            {
                String ForNoConnectionToThePowerAmplifierLetters = "нет связи с усилителем мощности литеры";
                byte[] iNumberForDescription = new byte[] { Convert.ToByte(i1 - 64) };
                BitArray btForFormula = new BitArray(iNumberForDescription);
                for (int i2 = 0; i2 < 5; i2++)
                    if (btForFormula[i2] == true)
                        ForNoConnectionToThePowerAmplifierLetters = String.Concat(ForNoConnectionToThePowerAmplifierLetters, " ", sForCombination[i2]);
                DictionaryOfCommandError.Add(Convert.ToByte(i1), ForNoConnectionToThePowerAmplifierLetters);

            }
            //
            DictionaryOfUniqueErrorForDURAT_PARAM_FWS = new Dictionary<byte, string>();
            DictionaryOfUniqueErrorForDURAT_PARAM_FWS.Add(2, "превышено кол-во частот в литере 1");
            DictionaryOfUniqueErrorForDURAT_PARAM_FWS.Add(3, "превышено кол-во частот в литере 2");
            DictionaryOfUniqueErrorForDURAT_PARAM_FWS.Add(4, "превышено кол-во частот в литере 3");
            DictionaryOfUniqueErrorForDURAT_PARAM_FWS.Add(5, "превышено кол-во частот в литере 4");
            DictionaryOfUniqueErrorForDURAT_PARAM_FWS.Add(6, "превышено кол-во частот в литере 5");
            //
            DictionaryOfUniqueErrorForPARAM_FHSS = new Dictionary<byte, string>();
            DictionaryOfUniqueErrorForPARAM_FHSS.Add(2, "значение мин. частоты вне диапазона");
            DictionaryOfUniqueErrorForPARAM_FHSS.Add(3, "значение макс. частоты вне диапазона");
            DictionaryOfUniqueErrorForPARAM_FHSS.Add(4, "неверный код модуляции ");
            DictionaryOfUniqueErrorForPARAM_FHSS.Add(5, "неверный код девиации");
            DictionaryOfUniqueErrorForPARAM_FHSS.Add(6, "неверный код манипуляции");
        }
        //ф-ия расшифровки
        public String DecodingError(byte[] ErrorInfo)//bCode, byte bError
        {
            if (DictionaryOfCommandError.ContainsKey(ErrorInfo[1]))
                return DictionaryOfCommandError[ErrorInfo[1]];
            else if (ErrorInfo[0] == DURAT_PARAM_FWS && DictionaryOfUniqueErrorForDURAT_PARAM_FWS.ContainsKey(ErrorInfo[1]))
                return DictionaryOfUniqueErrorForDURAT_PARAM_FWS[ErrorInfo[1]];
            else if (ErrorInfo[0] == PARAM_FHSS && DictionaryOfUniqueErrorForPARAM_FHSS.ContainsKey(ErrorInfo[1]))
                return DictionaryOfUniqueErrorForPARAM_FHSS[ErrorInfo[1]];
            else
                return "Unknown error number";
        }
        public bool IsNotThereError(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] == 0 || (ErrorInfo[1] >= 65 && ErrorInfo[1] <= 95) ? true : false);//ErrorInfo[1]==0 ? true : false
        }
        //ф-ия распоснозавания проблем с ответом от синтезатора формирователя литеры
        public bool IsThereProblemWithSynthesizer(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] >= 33 && ErrorInfo[1] <= 63 ? true : false);
        }
        //ф-ия распоснозавания номера литеры от, у которой нет связи c усилителем мощности лиеры
        public String[] WhichProblemWithSynthesizer(byte[] ErrorInfo)
        {
            String[] res = null;
            if (ErrorInfo[1] >= 33 && ErrorInfo[1] <= 63)
            {
                String sAnswer = DictionaryOfCommandError[ErrorInfo[1]];
                sAnswer = sAnswer.Replace("нет ответа от синтезатора формирователя литеры ", "");
                res = sAnswer.Split(' ');
            }
            return res;
        }
        //ф-ия распоснозавания проблем со связью c усилителем мощности лиеры
        public bool IsThereLinkWithAmplifaer(byte[] ErrorInfo)
        {
            return (ErrorInfo[1] >= 65 && ErrorInfo[1] <= 95 ? true : false);
        }
        public String[] WhichLinkProblemAmplifaersAnswer(byte[] ErrorInfo)
        {
            String[] res = null;
            if (ErrorInfo[1] >= 65 && ErrorInfo[1] <= 95)
            {
                String sAnswer = DictionaryOfCommandError[ErrorInfo[1]];
                sAnswer = sAnswer.Replace("нет связи с усилителем мощности литеры ", "");
                res = sAnswer.Split(' ');
            }
            return res;
        }
    }
}
