﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Net.NetworkInformation;

namespace WAVtranslation
{
    class UDP
    {
        //ПОЛЯ
        private int myPort;
        private string myIP;
        private int fpsPort;
        private string fpsIP;
        public static UdpClient socket;

        static IPEndPoint target;
        static IPEndPoint localPoint;


        //КОНСТРУКТОР
        public UDP(int myPort, string myIP, int fpsPort, string fpsIP)
        {
            this.myPort = myPort;
              this.fpsPort = fpsPort;

           
            this.fpsIP = fpsIP;
           
           this.myIP = myIP;
           
            

        }


        public void Connect()
        {
            try
            {
                
                target = new IPEndPoint(IPAddress.Parse(fpsIP), fpsPort);// отправка данных (для упрощения - на свой же комп, но другой порт)
                localPoint = new IPEndPoint(IPAddress.Parse(myIP), myPort);
                if (socket == null)
                { socket = new UdpClient(localPoint); }
                
                 socket.BeginReceive(new AsyncCallback(OnUdpData), socket); //почему в конце сокет?  Тут object - Пользовательский объект, который содержит информацию об операции приема. Этот объект передается requestCallbackделегату после завершения операции.

            }
            catch
            {
                Console.WriteLine("Соединение не установлено");
            }
            
        }
        

        //МЕТОДЫ

        //для асинхронной передчи/приема
        static void OnUdpData(IAsyncResult result)
        {
            UdpClient socket = result.AsyncState as UdpClient; // то, что указано в BeginReceive как 2й параметр
            IPEndPoint source = new IPEndPoint(0, 0);  // указывает на того, кто отправил сообщение
            FpsCommunication.message = socket.EndReceive(result, ref source); // получает текущее сообщение и заполняет источник
            ReturnMessage();
            socket.BeginReceive(new AsyncCallback(OnUdpData), socket); //планируем следующую оперцию приема после завершения чтения
          
        }

        public static bool ReturnMessage()
        {
            FpsCommunication.bYouCan = true;
            FpsCommunication.messageReceived = true;
          //  FpsCommunication.message = message;
            return true;
        }

        //отправка данных на другой оконечный узел
        public bool SendData(byte[] bSend)
        {
            try
            {
                socket.Send(bSend, bSend.Length,target);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }   
}
