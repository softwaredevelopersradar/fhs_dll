﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAVtranslation
{
    class DecodingFWS
    {
        static String[] Modulation = new String[] { "ЧМШ", "ЧМн", "ЧМ-4", "ЧМ-8", "ФМн", "ФМн-4", "ФМн-8", "КФМ" };//виды модуляции
        static String[] EffectiveDeviation1 = new String[]
        { "+/- 1.75", "+/- 2.47", "+/- 3.5", "+/- 4.2", "+/- 5", "+/- 7.07", "+/- 10", "+/- 15.8", "+/- 25",
            "+/- 35.35", "+/- 50", "+/- 70", "+/- 100", "+/- 150", "+/- 250", "+/- 350", "+/- 500", "+/- 700", "+/- 1000" };//для расшифровок активных девиаций для ЧМШ
        static String[] RangeParametersForAll = new String[] { "0.5", "1.0", "1.5", "2.0" };//параметры дальности
        static String[] EffectiveDeviation2 = new String[] { "2", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22", "24" };//для расшифровок активных девиаций для ЧМн, ЧМ-4б ЧМ-8, ФМн
        static String[] Durability5 = new String[] { "0.2", "0.5", "1.0", "5.0", "10", "20", "40", "80", "400" };//для ФМн, ФМ-4, ФМ-8
        static String[] ForFilling = new String[] { "\r\nВид модуляции - ", ",\r\nЭффективная девиация частоты, кГц - ", ",\r\nДлительность, мкс. - ", ",\r\nПараметры длительности, мс. - " };//для value
        String[] Durability2;
        public DecodingFWS()
        {
            Durability2 = new String[6];
            double dStart = 62.5;
            for (int i = 0; i < 6; i++)
            {
                Durability2[i] = Convert.ToString(dStart);
                dStart *= 2;
            }
        }
        public String Description(byte a1, byte a2, byte a3, byte a4)
        {
            String res = String.Concat(ForFilling[0], Modulation[a1 - 1]);
            switch (a1)
            {
                case 1:
                    res = String.Concat(res, ForFilling[1], EffectiveDeviation1[a2 - 1]);
                    break;
                case 2:
                case 3:
                case 4:
                    res = String.Concat(res, ForFilling[1], EffectiveDeviation2[a2 - 1], ForFilling[2], Durability2[a3 - 1]);
                    break;
                case 5:
                case 6:
                    res = String.Concat(res, ForFilling[2], Durability5[a3 - 1]);
                    break;
                case 7:
                    res = String.Concat(res, ForFilling[2], "0.2");
                    break;
                default:
                    break;
            }
            res = String.Concat(res, ForFilling[3], RangeParametersForAll[a4 - 1]);
            return res;
        }
    }
}
