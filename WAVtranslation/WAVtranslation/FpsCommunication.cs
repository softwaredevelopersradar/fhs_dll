﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using NAudio;
using NAudio.Wave;
using NAudio.CoreAudioApi;


namespace WAVtranslation
{
    class FpsCommunication
    {
        //ПОЛЯ
       
            // private string filepath = ""; //путь к файлу
        private int minbyteaudio = 128;  //минимальная длина фрагмента аудио для одной кодограммы
        public int numberOfDevice;
        public int bufferSize;

        private byte addressSender;
        private byte addressRecipient;

        static UDP udp;

        private byte[] frequency;


        Thread thrRead;                // поток приема кодограмм от ФП
        public static bool messageReceived;  //чтобы получить сообщение сначала в класс UDP, а потом передать его в этот

        private static int position = 0; //для прохождения по массиву файла
        private static byte[] file = null;  //файл без заголовка, т.е. с 44 байта
        public static byte[] message;   //сообщение, переданное с ФПС через класс UDP
        public static bool bYouCan;  //разрешает отправлять следующую кодограмму
        private bool voiceEnd = true;
      //  private static bool endtranslation;  //прерывает трансляцию
        Thread translthread;
        Thread onlinethread;
        static byte bOnlineORRecord;  //если 1 - Online, если 0 - Record

        //поток для нашей речи
        WaveIn waveIn = null;
        int sampleRate;
        int bitsInSample;
        static Queue<byte[]> packeges; //мб потом статик сделать?
        List<byte[]> forSend;
         List<byte[]> forFifo;

        //для счетчика кодограм
        private Semaphore smphForCount;
        byte bCountSendCmd;
        byte bCountReceiveCmd;
        byte bCountChiperSendCmd;

        int iAmauntOfErrorBytes;  //количество байт на ошибки в ответе от ФПС?

        static Timer Timer;
        static int losscount = 0;  //считает отсутствие ответа  
        static bool bTimeOut = false;
        public const int Infinite = -1;

        //КОНСТРУКТОРЫ
        public FpsCommunication(UDP tryudp, int AmauntOfErrorBytes, int frequency, byte addressSender, byte addressRecipient)
        {
            messageReceived = false;
            bYouCan = false;
            message = null;
            udp = tryudp;

            this.frequency = Converting(frequency);

            this.addressSender = addressSender;
            this.addressRecipient = addressRecipient;

            smphForCount = new Semaphore(0, 1);
            bCountSendCmd = bCountReceiveCmd = bCountChiperSendCmd = 0;

            thrRead = new Thread(new ThreadStart(ReceiveData));   //сперто из ConnectClient, возможно в другое мето надо 
            thrRead.IsBackground = true;// задаем поток фоновым
            thrRead.Start();// запускаем поток

            this.iAmauntOfErrorBytes = AmauntOfErrorBytes;

        }

        private byte[] Converting(int int_frequency)
        {
            byte[] tryfrequency = new byte[3];
            int foo = int_frequency;
            byte lolo = (byte)(foo & 0xff);
            byte hilo = (byte)((foo >> 8) & 0xff);
            byte lohi = (byte)((foo >> 16) & 0xff);
            tryfrequency[2] = lohi;
            tryfrequency[1] = hilo;
            tryfrequency[0] = lolo;
            return tryfrequency;
        }


        //СТРУКТУРЫ
        public struct TCodeErrorInform//изменено
        {
            public byte[] bCodeError;//для новых ошибок
            public byte[] bInform;
        }
        TCodeErrorInform tCodeErrorReadInform;

       

        private byte bLastStateQuery;            // номер литеры при запросе состояния//public
        private byte bLastPowerQuery;            // номер литеры при запросе мощности//public
        private byte bLastVoltageQuery;          // номер литеры при запросе напряжения//public
        private byte bLastCurrentQuery;          // номер литеры при запросе тока//public
        private byte bLastTempQuery;             // номер литеры при запросе температуры//public





        // хэштаблица для быстрого определения наличия
        HashSet<byte> CmdCodes = new HashSet<byte>(new byte[15] { 1, 2, 5, 6, 7, 8, 9, 10, 11, 12, 15, 16, 17, 18, 13 });
        enum CodeogramCiphers : byte // перечисление для шифров кодограм
        {
            REQUEST_STATE = 1,
            TYPE_LOAD,
            DURAT_PARAM_FWS = 5,
            REQUEST_VOLTAGE,
            REQUEST_POWER,
            REQUEST_CURRENT,
            REQUEST_TEMP,
            RADIAT_OFF,
            PARAM_FHSS,
            STOP_FHSS,//Выход из режима РП-ППРЧ
            REQUEST_FPS_STATE,
            SET_FHSS_PARAMS_DURAT,// установить параметры ППРЧ для измерения длительности излучения
            RESET,
            ReadinessReceptionSLI, // запрос на отправку речеподобной помехи
            ParamsSLI,   //отправка кодограммы с помехой
            PARAM_FHSSForMoreOneNetwork
        }

        //СОБЫТИЯ
        // наличие (отсутствие) подключения
        public delegate void WithoutParamsEventHandler(); //делегат обработка события подключения
       // public event WithoutParamsEventHandler ConnectNet;// событие подключния, связанное с делегатом ConnectEventHandler
       // public event WithoutParamsEventHandler DisconnectNet;// событие отключения
        public event WithoutParamsEventHandler LostAnyCmd;//Утеряна кодограмма
        public event WithoutParamsEventHandler LostFPS;  //фпс не отвечает
        protected void OnWithoutParamsEvent(WithoutParamsEventHandler some_event)
        {
            if (some_event != null)
                some_event();//Raise the event
        }
        delegate void FillMainParams(ref byte[] ArrForSend);//мой делегат для общей части


        public delegate void ReceiveOrSendEventHandler(byte bCode, object obj);//делегат обработки события получения кодограммы
        public event ReceiveOrSendEventHandler ReceiveCmd;// событие получения команды
        public event ReceiveOrSendEventHandler SendCmd; // отправлена кодограмма
        protected virtual void OnReceiveOrSendCmd(ReceiveOrSendEventHandler some_ev, byte bCode, object obj)
        {
            if (some_ev != null)
                some_ev(bCode, obj);//Raise the event
        }

        public delegate void ByteArrayEventHandler(byte[] bByte);
        public event ByteArrayEventHandler ReceiveByte; //получен массив байт
        public event ByteArrayEventHandler SendByte;// отпрвлен массив байт
        protected virtual void OnByteArray(ByteArrayEventHandler some_ev, byte[] bByte)
        {
            if (some_ev != null)
                some_ev(bByte);//Raise the event
        }

        public delegate void UpdateEventHandler(byte bLetter, TCodeErrorInform tCodeErrorInform);
        public event UpdateEventHandler UpdateState;
        public event UpdateEventHandler UpdatePower;
        public event UpdateEventHandler UpdateVoltage;
        public event UpdateEventHandler UpdateCurrent;
        public event UpdateEventHandler UpdateTemp;
        protected void OnUpdateSmth(UpdateEventHandler some_ev, byte bLetter, TCodeErrorInform tCodeErrorInform)
        {
            if (some_ev != null)
                some_ev(bLetter, tCodeErrorInform);//Raise the event
        }


        //ХАЙ ПОКА БУДЕТ, НО НАДО ПЕРЕСМОТРЕТЬ
        ////public void ChangingAfterCatchLoss()
        ////{
        ////    bCountReceiveCmd = 0;
        ////    bCountSendCmd = 0;
        ////}
        ////private void CatchesLosses(byte CountOfRecisevedCMD)//менять !!! НАДО ЛИ ЭТО ВООБЩЕ для речеподобной?
        ////{
        ////    smphForCount.Release();
        ////    bCountReceiveCmd = Convert.ToByte((bCountReceiveCmd == 255 ? 0 : bCountReceiveCmd + 1));
        ////    if (bCountReceiveCmd != bCountSendCmd && CountOfRecisevedCMD == bCountChiperSendCmd)
        ////    {
        ////        OnWithoutParamsEvent(LostAnyCmd);
        ////        ChangingAfterCatchLoss();
        ////    }
        ////    smphForCount.WaitOne();
        ////}
        void FillingForTCodeErrorInform(byte NumberOfCMD, byte[] bRead)//сделать под оба протокола   МОЖЕТ НЕ НАДО 2 ВИДА РАЗНЫХ ОШИБОК ДЕЛАТЬ?
        {
            tCodeErrorReadInform.bInform = (bRead.Length > (5 + iAmauntOfErrorBytes) ? new byte[bRead.Length - (5 + iAmauntOfErrorBytes)] : null);
            if (iAmauntOfErrorBytes == 1)//так сделано, потому что для исходного протокола есть некоторые различия в кодах ошибок для разных кодограмм 
                tCodeErrorReadInform.bCodeError = new byte[2] { bRead[2], bRead[5] };
            else if (iAmauntOfErrorBytes == 3)
            {
                tCodeErrorReadInform.bCodeError = new byte[iAmauntOfErrorBytes];// код ошибки
                Array.Copy(bRead, 5, tCodeErrorReadInform.bCodeError, 0, iAmauntOfErrorBytes);
            }
            if (bRead.Length > 5 + iAmauntOfErrorBytes)
                Array.Copy(bRead, 5 + iAmauntOfErrorBytes, tCodeErrorReadInform.bInform, 0, bRead.Length - (5 + iAmauntOfErrorBytes));
            //OnReceiveOrSendCmd(ReceiveCmd, NumberOfCMD, tCodeErrorReadInform);// передать параметры в событие ReceiveCMD// подтверждение принятия !!!
            ////CatchesLosses(bRead[3]);//передется счетчик кодограм
        }


        //контейнер кодограммы 
        bool CommanPartForCodograms(int CodogramaLenght, byte bCipher, FillMainParams some_del, Object for_some_del)
        {
            //общая часть: заполнение служебной части кодограммы
            byte[] bArreyForSend = new byte[CodogramaLenght];
            bArreyForSend[0] = addressSender;    
            bArreyForSend[1] = addressRecipient;  
            bArreyForSend[2] = bCipher;
            smphForCount.Release();
            bCountChiperSendCmd = Convert.ToByte((bCountChiperSendCmd == 255 ? 0 : bCountChiperSendCmd + 1));
            bArreyForSend[3] = bCountChiperSendCmd;
            smphForCount.WaitOne();
            bArreyForSend[4] = Convert.ToByte(bArreyForSend.Length - 5);
            //место для делегата, который заполнит или нет (если null) информационную часть, различную практически для всех кодограмм
            if (some_del != null)
                some_del.Invoke(ref bArreyForSend);
            //снова общая часть
            if (udp.SendData(bArreyForSend))
            {
                OnReceiveOrSendCmd(SendCmd, bCipher, for_some_del);//отследить вторую часть
                OnByteArray(SendByte, bArreyForSend);
                smphForCount.Release();
                bCountSendCmd = Convert.ToByte((bCountSendCmd == 255 ? 0 : bCountSendCmd + 1));
                smphForCount.WaitOne();
                return true;
            }
            else
                return false;
        }

        //получение данных
        public void ReceiveData()
        {
            while (true) // работает постоянно
            {
                if (messageReceived)
                {
                    byte[] bRead = message;

                    if (bRead?.Length >= 5)//не должно быть короче 5
                    {
                        byte bCode = bRead[2];// шифр кодограммы
                        //поменять по звуку
                        if (CmdCodes.Contains(bCode))//если есть такая кодограмма
                        {
                            FillingForTCodeErrorInform(bCode, bRead);
                            OnByteArray(ReceiveByte, bRead);
                            //сделать tuple
                            if (bCode == (byte)CodeogramCiphers.REQUEST_STATE)
                                OnUpdateSmth(UpdateState, bLastStateQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_VOLTAGE)
                                OnUpdateSmth(UpdateVoltage, bLastVoltageQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_POWER)
                                OnUpdateSmth(UpdatePower, bLastPowerQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_CURRENT)
                                OnUpdateSmth(UpdateCurrent, bLastCurrentQuery, tCodeErrorReadInform);
                            else if (bCode == (byte)CodeogramCiphers.REQUEST_TEMP)
                                OnUpdateSmth(UpdateTemp, bLastTempQuery, tCodeErrorReadInform);
                        }
                    }
                    messageReceived = false;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }


        //вызывается для отправки записи
        public void StartSendVoice(string filepath)
        {
            bOnlineORRecord = 0;
            voiceEnd = false;

            byte[] fullfile = System.IO.File.ReadAllBytes(filepath);
            file = new byte[fullfile.Length - 44];
            Array.Copy(fullfile, 44, file, 0, fullfile.Length-44);
            

            if (translthread == null)
            {
                
                Thread translthread = new Thread(new ThreadStart(SendVoise));
                translthread.IsBackground = true;
                translthread.Start();
            }
        }

        //возвращает фрагмент аудио длиной minbyteaudio байт, начиная с позиции position
        private byte[] CopyPartOfRecord()
        {
            byte[] partOfRecord = new byte[minbyteaudio];
            int difference = file.Length - 1 - position;
            if (difference < minbyteaudio)
            {
                Array.Copy(file, position, partOfRecord, 0, difference);
                Array.Copy(Enumerable.Repeat((byte)0, minbyteaudio - difference).ToArray(), 0, partOfRecord, difference - 1, minbyteaudio - difference); //дополним нулями последнюю порцию аудио до 255
            }
            else
            {
                Array.Copy(file, position, partOfRecord, 0, partOfRecord.Length);
            }
            position += minbyteaudio;
            if (position >= file.Length)
            {
                position -= file.Length;
            }
            return partOfRecord;
        }

        private void LossesCount(object state)
        {
            if (!bYouCan)
            { losscount += 1;
              bTimeOut = true;
            }
           
        }
        

        //отправляет кусочки аудио по приходу ответа от ФПС + выбрасывает event LostFPS при отсутсвии ответов
        private void SendVoise ()
        {
            TimerCallback lossesCount = new TimerCallback(LossesCount);
            Timer = new Timer(lossesCount, bTimeOut, Infinite, Infinite); //таймер для ожидания ответа
            
            int step = 0;  //нужен для подсчета отсутствующих ответов (отсчитывает 3 шага подряд, чтобы выкинуть ошибку)

            while (!voiceEnd)
            {
                //если не пришел 1 ответ, начать отсчет шагов (до 3)
                if (losscount == 1)
                {
                    step = 1;
                }

                //отправка аудио (128 байт) по приходу ответа от ФПС или по окончанию таймера 
                if (bYouCan||bTimeOut)
                {
                    Timer.Change(Infinite, Infinite);
                    bTimeOut = false;
                    switch (bOnlineORRecord)
                    {
                        case 0:
                            //ДОБАВИТЬ С КАКОГО МЕСТА НАЧАТЬ ПРОИГРЫШ
                            message = CopyPartOfRecord();
                            SendPartOfRecord(message);  //для файла   
                            break;
                        case 1:
                            if (packeges.Count != 0)
                            {
                                 SendPartOfRecord(packeges.Dequeue());  //online
                            }
                            break;
                    }
                    bYouCan = false;
                    Timer.Change(140,Infinite);
                    step++;
                }

                //проверка на 3 подряд не пришедших ответа
                if ((losscount == 3) && (step == losscount))
                {
                    OnWithoutParamsEvent(LostFPS);  //если не пришли три подряд ответа, кидаем исключение
                    losscount = 0;
                    voiceEnd = true;
                }

                //обнуление счетчиков, если не 3 шага подряд не пришел ответ
                if (step == 3|| losscount == 3)
                {
                    step = 0;
                    losscount = 0;
                }
                Thread.Sleep(1);
            }
        }

        //останавливает поток отпрвки аудиозаписи
        public void StopTranslation()
        {
            voiceEnd = true;
            if (Timer != null)
            { Timer.Dispose(); }
            
        }

        //перегрузка 2 для отправки речи Online
        public void StartSendVoice(int samlerate, int bitsinsamle)
        {
            bOnlineORRecord = 1;
            voiceEnd = false;
            this.bitsInSample = bitsinsamle;
            this.sampleRate = samlerate;

            packeges = new Queue<byte[]>();
            forSend = new List<byte[]>();
            forFifo = new List<byte[]>();
           

            StartOnlineStream();
            if (onlinethread == null)
            {
                Thread onlinethread = new Thread(new ThreadStart(SendVoise));
                onlinethread.IsBackground = true;
                onlinethread.Start();
            }
            
        }
        
        //возвращает фрагмент online-речи длиной minbyteaudio байт
        private void StartOnlineStream()
        {
            try
            {
                byte[] partOfStream = new byte[minbyteaudio];
                //создаем поток для записи нашей речи

                //ReceiveCmd(4, this);

                waveIn = new WaveIn();
                waveIn.DeviceNumber = numberOfDevice;

                waveIn.BufferMilliseconds = bufferSize; //выбирать число, кратное 32

                //Формат wav-файла - принимает параметры - частоту дискретизации и количество каналов(здесь mono)
                // waveIn.WaveFormat = new WaveFormat(sampleRate, bitsInSample, 1);
                //Прикрепляем к событию DataAvailable обработчик, возникающий при наличии записываемых данных
                waveIn.WaveFormat = new WaveFormat(8000, 8, 1); //первое - частота дискретизации, 8 - кол-во бит в сэмпле, 1 - кол-во каналов
                waveIn.DataAvailable += new EventHandler<WaveInEventArgs>(waveIn_DataAvailable);
                //Прикрепляем обработчик завершения записи
                waveIn.RecordingStopped += new EventHandler<StoppedEventArgs>(waveIn_RecordingStopped);
                //Начало записи
                waveIn.StartRecording();
                //ReceiveCmd(5, this);


            }
            catch(SystemException ex)
            {
                
                 //ReceiveCmd(7, (Object)(ex.Message));
               // ReceiveCmd(7, (Object)ex.Message);
                // ReceiveCmd(7, this);
            }
            
        }

        void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            // ReceiveCmd(6, this);

            byte[] ForBuffer = new byte[e.BytesRecorded]; //порция данных (1024 байт, если размер буфера 128мс)
            Array.Copy(e.Buffer, 0, ForBuffer, 0, e.BytesRecorded); //копирование данных с аудио карты из буфера в эту порцию
            ToFifo(ForBuffer, e.BytesRecorded);
        }

        //Разбиение порции данных с аудиокарты на массивы по 128 байт
        void ToFifo(byte[] ForBuffer, int partOFByte)
        {
            byte[] partFifo = new byte[minbyteaudio];
            for (int i = 0; i < ForBuffer.Length; i += minbyteaudio)
            {
                int difference = ForBuffer.Length - i;
                if (difference < minbyteaudio)
                {
                    break;
                }
                else
                {
                    Array.Copy(ForBuffer, i, partFifo, 0, minbyteaudio);
                    packeges.Enqueue(partFifo.Clone() as byte[]);
                }
            }
        }

        private void waveIn_RecordingStopped(object sender, EventArgs e)
        {
                waveIn.Dispose();
                waveIn = null;
            //bIsRecordingNow = false;//подумай, почему так
        }

        public void StopOnline()
        {
            voiceEnd = true;
            if (waveIn != null)
            {
                waveIn.StopRecording();
            }
        }

        //КОДОГРАММЫ

        // (шифр 16) запрос на переход в режим речеподобной помехи или его отключение(пока что на кнопке подключения)
        public bool SendConfirmation(byte bMode)
        {
            return CommanPartForCodograms(9, (byte)CodeogramCiphers.ReadinessReceptionSLI,
                delegate (ref byte[] ForSend)
                {
                    ForSend[5] = this.frequency[0];
                    ForSend[6] = this.frequency[1];
                    ForSend[7] = this.frequency[2];
                    ForSend[8] = bMode;
                },
            null);
        }

        // (шифр 17) данные речеподобной помехи
        private bool SendPartOfRecord(byte[] RecordForSending)
        {
            return CommanPartForCodograms(5 + minbyteaudio, (byte)CodeogramCiphers.ParamsSLI,
                delegate (ref byte[] ForSend)
                {
                    Array.Copy(RecordForSending, 0, ForSend, 5, minbyteaudio);   
                },
                RecordForSending);  
        }
    }
}
