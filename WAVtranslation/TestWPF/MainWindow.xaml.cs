﻿using System;
using System.Collections.Generic;
using System.Windows;
using FPSLib;
using ModelsTablesDBLib;

namespace TestWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string filepath = ""; //путь к файлу
        private int myport;   //порт этого устройства
        private int fpsport;  //порт ФПС
        private string myIP;  //IP этого устройства
        private string fpsIP; //IP ФПС
        private int frequency;
        private int device1;
        private FPS fps;



        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            myport = Convert.ToInt32(textMyPort.Text);
            fpsport = Convert.ToInt32(textFpsPort.Text);
            myIP = textMyIP.Text;
            fpsIP = textFpsIP.Text;
            frequency = Convert.ToInt32(textFreq.Text);
            fps = new FPS(myport, myIP, fpsport, fpsIP, 3);

            //fps.DisconnectNet += Warn;

            fps.Connect();
        }

        private void BtnRequest_Click(object sender, RoutedEventArgs e)
        {
            fps.GetStateAP(new List<byte> {1,2,3,5});
        }

        private void Warn(object obj, EventArgs e)
        {
            MessageBox.Show("Работает");
        }

        private void BtnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            fps.Disconnect();
        }

        private void BtnPath_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = "WAVE Audio File (.wav)|*.wav";
            
            if (dialog.ShowDialog() == true)
            {
                filepath = dialog.FileName;   //указываем путь к файлу
                textPath.Text = dialog.FileName;
            }
        }

        private void BtnStartRecord_Click(object sender, RoutedEventArgs e)
        {
            if (fps.SetVoice(filepath))
            { fps.RegimeVoice(1, frequency, 1); }

        }

        private void BtnStopRecord_Click(object sender, RoutedEventArgs e)
        {
            fps.RegimeVoice(0, frequency, 1);
        }

        private void BtnStartOnline_Click(object sender, RoutedEventArgs e)
        {
            device1 = Convert.ToInt32(textDevice.Text);
            byte device = Convert.ToByte(device1);
            if (fps.SetVoice(device))
            { fps.RegimeVoice(1, frequency, 1); }
        }

        private void BtnStopOnline_Click(object sender, RoutedEventArgs e)
        {
            fps.RegimeVoice(0, frequency, 1);
        }

        private void Btn_RadiatOff_Click(object sender, RoutedEventArgs e)
        {
            fps.SetRadiatOff();
        }

        private void BtnDurFWS_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                InterferenceParam lib = new InterferenceParam
                {
                    Modulation = 9,
                    Deviation = 0,
                    Manipulation = 0,
                    Duration = 0
                };
                TableSuppressFWS tDurationParamFWS = new TableSuppressFWS()
                {
                    FreqKHz = 35000000,
                    InterferenceParam = lib
                };
                List<TableSuppressFWS> list;
                list = new List<TableSuppressFWS> { tDurationParamFWS };
                fps.SetParamFWS(list, 500);
            }
            catch (Exception ex)
            {
            }
        }

        private void BtnFHSS_Click(object sender, RoutedEventArgs e) //фпс не отвечает
        {
            
            TableSuppressFHSS lit1 = new TableSuppressFHSS()
            {
                FreqMinKHz = 30000,
                InterferenceParam = new InterferenceParam
            {
                Modulation =9,
                Deviation = 0,
                Manipulation = 0
            }
            };
            TableSuppressFHSS lit2 = new TableSuppressFHSS()
            {
                FreqMinKHz = 55000,
                InterferenceParam = new InterferenceParam
                {
                    Modulation = 9,
                    Deviation = 0,
                    Manipulation = 0
                }
            };
            TableSuppressFHSS lit3 = new TableSuppressFHSS()
            {
                FreqMinKHz = 300000,
                InterferenceParam = new InterferenceParam
                {
                    Modulation = 9,
                    Deviation = 0,
                    Manipulation = 0
                }
            };
            TableSuppressFHSS lit4 = new TableSuppressFHSS()
            {
                FreqMinKHz = 540000,
                InterferenceParam = new InterferenceParam
                {
                    Modulation = 9,
                    Deviation = 0,
                    Manipulation = 0
                }
            };
            fps.SetParamFHSS(new List<TableSuppressFHSS> { lit1, lit2 , lit3 , lit4 }, 500, 1); 
        }

        private void BtnVoltage_Click(object sender, RoutedEventArgs e)
        {
            fps.GetVoltageAP(new List<byte> { 3 });
        }

        private void BtnPower_Click(object sender, RoutedEventArgs e)
        {
            fps.GetPowerAP(new List<byte> { 3});
        }

        private void BtnCurrent_Click(object sender, RoutedEventArgs e)
        {
            fps.GetCurrentAP(new List<byte> {3 });
        }

        private void BtnTemp_Click(object sender, RoutedEventArgs e)
        {
            fps.GetTempAP(new List<byte> {3 });
        }
        //1, 2, 3, 4, 5, 6, 7, 8, 9, 10

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            fps.SetTypeLoad(1,0);
        }

        private void BtnResetError_Click(object sender, RoutedEventArgs e)
        {
            fps.ResetError();
        }

        private void BtnStopFHSS_Click(object sender, RoutedEventArgs e)
        {
            fps.StopFHSS();
        }

        private void BtnStateFHS_Click(object sender, RoutedEventArgs e)
        {
            fps.StateFHS();
        }

        private void BtnSetPower_Click(object sender, RoutedEventArgs e) //фпс не отвечает
        {
            var list = new List<SetPowerLetter>
            { new SetPowerLetter { Letter = 1, Power = 1 },  new SetPowerLetter { Letter = 2, Power = 1 }};
            fps.SetPowerAP(list);
        }
    }
}
