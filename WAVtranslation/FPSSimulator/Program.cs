﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Media;
using System.IO;
using System.Threading;



namespace FPSSimulator
{
    class Program
    {
        static byte[] request;
        byte btargetIP = 1;//public      //хз
        byte blocalIP = 2;//public       //хз

        private Semaphore smphForCount;
        static byte bCountSendCmd = 0;
        static byte bCountReceiveCmd = 0;
        static byte bCountChiperSendCmd = 0;
        

        static void OnUdpData(IAsyncResult result)
        {
            // то, что указано в BeginReceive как 2й параметр:
            UdpClient socket = result.AsyncState as UdpClient;
            // указывает на того, кто отправил сообщение:
            IPEndPoint source = new IPEndPoint(0, 0);
            // получает текущее сообщение и заполняет источник:
            byte[] message = socket.EndReceive(result, ref source);
            // делай, что хочешь с `message` тут:
            //  Console.WriteLine("Got " + Encoding.ASCII.GetString(message) + " bytes from " + source);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Got " + message[2] + " codogram from " + source + ", countCMD = " + message[3]);
            request = SendRequest(message[2]);
            
            socket.Send(request, request.Length, source);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Send " + request[2] + " codogram to " + source + ", countCMD = " + request[3]);
            //планируем следующую оперцию приема после завершения чтения:
            socket.BeginReceive(new AsyncCallback(OnUdpData), socket);
        }
        static void Main(string[] args)
        {
            IPEndPoint local = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9114);
            UdpClient socket = new UdpClient(local); // `new UdpClient()` to auto-pick port
            // планируем первую операцию приема
            socket.BeginReceive(new AsyncCallback(OnUdpData), socket);
            // отправка данных (для упрощения - на свой же комп):
            IPEndPoint target = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9108);
            // отправить пару сообщений для примера:
            //for (int num = 1; num <= 3; num++)
            //{
            //    byte[] message = new byte[num];
            //    socket.Send(message, message.Length, target);
            //}
            Console.ReadKey();
        }
        static byte[] SendRequest(byte bCipher)
        {
            Program p = new Program();
            p.smphForCount = new Semaphore(0, 1);
            byte[] bArreyForSend = new byte[8];
            bArreyForSend[0] = p.blocalIP;   
            bArreyForSend[1] = p.btargetIP;   
            bArreyForSend[2] = bCipher;
            p.smphForCount.Release();
            bCountChiperSendCmd = Convert.ToByte((bCountChiperSendCmd == 255 ? 0 : bCountChiperSendCmd + 1));
            bArreyForSend[3] = bCountChiperSendCmd;
            p.smphForCount.WaitOne();
            bArreyForSend[4] = Convert.ToByte(bArreyForSend.Length - 5);
            Array.Copy(Enumerable.Repeat((byte)0, 3).ToArray(), 0, bArreyForSend, 5, 3); //для упрощения кидаем код ошибки 0
            return bArreyForSend;
        }
    }
}
