﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace FPSLib
{
    public class UDP
    {
        private readonly int _myPort;
        private readonly string _myIp;
        private readonly int _fpsPort;
        private readonly string _fpsIp;

        private UdpClient _udpClient;
        private IPEndPoint _remoteIpEndPoint;
        private IPEndPoint _localIpEndPoint;
        private Thread readDataThread;

        internal byte[] dataRead = null;  

        protected static bool dataReceivedFlag = false;  //получено сообщение от ФПС
        protected static bool youCanSendFlag = false;  //разрешает отправлять следующую кодограмму
        private bool stopFlag = true;  //прекратить поток чтения

        protected UDP(int myPort, string myIP, int fpsPort, string fpsIP)
        {
            _myPort = myPort;
            _fpsPort = fpsPort;
            _fpsIp = fpsIP;
            _myIp = myIP;
        }


        public event EventHandler ConnectNet;
        public event EventHandler ConnectFailed;
        public event EventHandler DisconnectNet;
        public event EventHandler<byte[]> SendByte;

        protected virtual void OnWithoutParam(EventHandler some_ev) => some_ev?.Invoke(this, null);
        protected virtual void OnByteArray(EventHandler<byte[]> some_ev, byte[] data) => some_ev?.Invoke(this, data);


        public void Connect()
        {
            try
            {
                if (_udpClient == null)
                {
                    _remoteIpEndPoint = new IPEndPoint(IPAddress.Parse(_fpsIp), _fpsPort);
                    _localIpEndPoint = new IPEndPoint(IPAddress.Parse(_myIp), _myPort);
                    if (_udpClient == null)
                    {
                        _udpClient = new UdpClient(_localIpEndPoint);  //открывать сокет по связке ЛОКАЛЬНОГО адреса и порта! 
                        OnWithoutParam(ConnectNet);
                    }
                    stopFlag = false;

                    if (readDataThread == null)
                    {
                        Thread readDataThread = new Thread(Read);
                        readDataThread.IsBackground = true;
                        readDataThread.Start();
                    }
                }
            }
            catch
            {
                OnWithoutParam(ConnectFailed);
            }
        }

        //Закрытие сокета
        public void Disconnect()
        {
            stopFlag = true;
            if (readDataThread != null)
            {
                readDataThread.Abort();
                readDataThread.Join(500);
                readDataThread = null;
            }
            if (_udpClient != null)
            {
                _udpClient.Close();
                _udpClient = null;
                OnWithoutParam(DisconnectNet); 
            }
        }

        //Прием данных по UDP
        private void Read()
        {
            while (!stopFlag)
            {
                try
                {
                    dataRead = _udpClient.Receive(ref _localIpEndPoint);
                    ReturnMessage();
                }
                catch { }
            }
        }

        //Выставление флагов по приходу данных от удаленного узла
        private static bool ReturnMessage()
        {
            youCanSendFlag = true;
            dataReceivedFlag = true;
            return true;
        }

        //Отправка данных на другой оконечный узел
        protected bool SendData(byte[] bSend)
        {
            try
            {
                _udpClient.Send(bSend, bSend.Length, _remoteIpEndPoint);
                OnByteArray(SendByte, bSend);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
