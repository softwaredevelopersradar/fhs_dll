﻿using System;

namespace FPSLib
{
    public class SendDataProcessing : ReceivedDataProcessing
    {
        private byte _addressSender;
        private byte _addressRecipient;

        protected SendDataProcessing(int myPort, string myIP, int fpsPort, string fpsIP, int iAmauntOfErrorBytes)
            : base(myPort, myIP, fpsPort, fpsIP, iAmauntOfErrorBytes)
        {
            _addressSender = 2;
            _addressRecipient = 4;
        }


        protected delegate void FillMainParams(ref byte[] ArrForSend); //делегат для общей части
        public event EventHandler<byte> SendCmd; // отправлена кодограмма
        protected virtual void OnSendCmd(EventHandler<byte> some_ev, byte code)=>some_ev?.Invoke(this, code);


        //Запаковка данных в кодограмму
        protected bool CommanPartForCodograms(int CodogramaLenght, byte cipher, FillMainParams some_del, object for_some_del)
        {
            //общая часть: заполнение служебной части кодограммы
            byte[] arreyForSend = new byte[CodogramaLenght];
            arreyForSend[0] = _addressSender;
            arreyForSend[1] = _addressRecipient;
            arreyForSend[2] = cipher;
            countChiperSendCmd = Convert.ToByte((countChiperSendCmd == 255 ? 0 : countChiperSendCmd + 1));
            arreyForSend[3] = countChiperSendCmd;
            arreyForSend[4] = Convert.ToByte(arreyForSend.Length - 5);
            //место для делегата, который заполнит или нет (если null) информационную часть, различную практически для всех кодограмм
            if (some_del != null)
                some_del.Invoke(ref arreyForSend);
            //снова общая часть
            if (SendData(arreyForSend))
            {
                OnSendCmd(SendCmd, cipher);
                countSendCmd = Convert.ToByte((countSendCmd == 255 ? 0 : countSendCmd + 1));
                return true;
            }
            
            else
                return false;
        }
    }
}
