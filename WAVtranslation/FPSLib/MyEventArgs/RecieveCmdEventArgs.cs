﻿
namespace FPSLib
{
    public struct RecieveCmdEventArgs
    {
        public byte CommandCode { get; private set; }
        public ErrorDataStructure ErrorDataStructure { get; private set; }

        public RecieveCmdEventArgs(byte commandCode, ErrorDataStructure errorDataStructure)
        {
            CommandCode = commandCode;
            ErrorDataStructure = errorDataStructure;
        }
    }
}