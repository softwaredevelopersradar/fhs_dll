﻿using System.Collections.Generic;

namespace FPSLib
{
    public struct TwoValEventArgs
    {
        public ErrorDataStructure Errors;
        public List<VoltageParam> Voltages;
        public TwoValEventArgs(ErrorDataStructure errors, List<VoltageParam> voltages)
        {
            Errors = errors;
            Voltages = voltages;
        }
    }
}
