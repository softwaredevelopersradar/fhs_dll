﻿using System.Collections.Generic;

namespace FPSLib
{
    public struct ThreeValEventArgs
    {
        public ErrorDataStructure Errors;
        public List<PowerParam> Power;
        public ThreeValEventArgs(ErrorDataStructure errors, List<PowerParam> power)
        {
            Errors = errors;
            Power = power;
        }
    }
}