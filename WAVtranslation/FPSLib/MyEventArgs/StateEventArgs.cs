﻿namespace FPSLib
{
    public struct StateEventArgs
    {
        public ErrorDataStructure Errors { get; private set; }
        public byte Version { get; private set; }
        public byte ModeFps { get; private set; }

        public StateEventArgs(ErrorDataStructure errors, byte modeFPS, byte version)
        {
            Errors = errors;
            ModeFps = modeFPS;
            Version = version;
        }
    }
}