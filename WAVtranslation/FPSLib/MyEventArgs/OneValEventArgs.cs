﻿namespace FPSLib
{
    public struct OneValEventArgs
    {
        public ErrorDataStructure Error;
        public byte[] State;
        public OneValEventArgs(ErrorDataStructure error, byte[] state)
        {
            Error = error;
            State = state;
        }
    }
}
