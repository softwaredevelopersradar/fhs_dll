﻿using System;
namespace FPSLib
{
    public struct FiveParam
    {
        public byte Letter { get; set; }
        public byte Value1 { get; set; }
        public byte Value2 { get; set; }
        public byte Value3 { get; set; }
        public byte Value4 { get; set; }

        public FiveParam(byte letter, byte value1, byte value2, byte value3, byte value4)
        {
            Letter = letter;
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
            Value4 = value4;
        }

        public FiveParam(byte[] bytes, int startIndex = 0)
        {
            try
            {
                Letter = bytes[startIndex];
                Value1 = bytes[startIndex + 1];
                Value2 = bytes[startIndex + 2];
                Value3 = bytes[startIndex + 3];
                Value4 = bytes[startIndex + 4];
            }
            catch
            {
                Letter = 0;
                Value1 = 0;
                Value2 = 0;
                Value3 = 0;
                Value4 = 0;
            }
        }

        public FiveParam(byte[] bytes, int i, int startIndex = 0)
        {
            try
            {
                Letter = Convert.ToByte(i);
                Value1 = bytes[startIndex];
                Value2 = bytes[startIndex + 1];
                Value3 = bytes[startIndex + 2];
                Value4 = bytes[startIndex + 3];
            }
            catch
            {
                Letter = 0;
                Value1 = 0;
                Value2 = 0;
                Value3 = 0;
                Value4 = 0;
            }
        }
    }
}