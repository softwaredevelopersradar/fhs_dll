﻿namespace FPSLib
{
    public struct ErrorDataStructure
    {
        public byte ErrorCode1 { get; private set; }
        public byte ErrorCode2 { get; private set; }
        public byte ErrorCode3 { get; private set; }

        public ErrorDataStructure(byte errorCode1, byte errorCode2, byte errorCode3)
        {
            ErrorCode1 = errorCode1;
            ErrorCode2 = errorCode2;
            ErrorCode3 = errorCode3;
        }

        public ErrorDataStructure(byte[] errorBytes, int startIndex = 0)
        {
            try
            {
                ErrorCode1 = errorBytes[startIndex];
                ErrorCode2 = errorBytes[startIndex + 1];
                ErrorCode3 = errorBytes[startIndex + 2];
            }
            catch
            {
                ErrorCode1 = 0;
                ErrorCode2 = 0;
                ErrorCode3 = 0;
            }
        }
    }
}
