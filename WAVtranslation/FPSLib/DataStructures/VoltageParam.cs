﻿using System;

namespace FPSLib
{
    public struct VoltageParam
    {
        public byte Letter { get; set; }
        public ushort Voltage { get; set; }

        public VoltageParam(byte letter, ushort voltage)
        {
            Letter = letter;
            Voltage = voltage;
        }

        public VoltageParam(byte[] bytes, int startIndex = 0)
        {
            try
            {
                Letter = bytes[startIndex];
                Voltage = BitConverter.ToUInt16(bytes, startIndex + 1);
            }
            catch
            {
                Letter = 0;
                Voltage = 0;
            }
        }

        public VoltageParam(byte[] bytes, int i, int startIndex = 0)
        {
            try
            {
                Letter = Convert.ToByte(i+1);
                Voltage = BitConverter.ToUInt16(bytes, startIndex);
            }
            catch
            {
                Letter = 0;
                Voltage = 0;
            }
        }
    }
}
