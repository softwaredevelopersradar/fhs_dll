﻿using System;
namespace FPSLib
{
    public struct PowerParam
    {
        public byte Letter { get; set; }
        public byte Power { get;  set; }
        public byte CommandCode { get; set; }

        public PowerParam(byte letter, byte power, byte commandCommandCode)
        {
            Letter = letter;
            Power = power;
            CommandCode = commandCommandCode;
        }

        public PowerParam(byte[] bytes, int startIndex = 0)
        {
            try
            {
                Letter = bytes[startIndex];
                Power = bytes[startIndex + 1];
                CommandCode = bytes[startIndex + 2];
            }
            catch
            {
                Letter = 0;
                Power = 0;
                CommandCode = 0;
            }
        }
        public PowerParam(byte value, int i)
        {
            try
            {
                Letter = Convert.ToByte(i+1);
                Power = value;
                CommandCode = 0;
            }
            catch
            {
                Letter = 0;
                Power = 0;
                CommandCode = 0;
            }
        }
    }
}