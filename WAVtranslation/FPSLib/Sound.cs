﻿using System;
using System.Collections.Generic;
using System.Linq;
using NAudio.Wave;
using System.Threading;
using System.Threading.Tasks;

namespace FPSLib
{
    public class Sound : SendDataProcessing
    {
        protected bool soundONFlag = false; //для проверки играет ли уже звуковая помеха

        protected static byte bOnlineORRecord;  //если 1 - Online, если 0 - Record
        protected bool voiceEndFlag = true; //флаг окончания трансляции
        private const int _minbyteaudio = 128;  //минимальная длина фрагмента аудио для одной кодограммы  
        private byte[] _frequencyKHz; //частота вещания помехи
        protected Thread soundThread; 

        protected WaveInEvent waveIn = null;  //поток для микрофона
        protected byte numberOfDevice = 0; 
        private const int _bufferSizeMs = 128; //размер буфера для считывания с аудиокарты (выбирать число, кратное 32!!!)
        protected const int sampleRate = 8000;
        protected const int bitsInSample = 8;
        protected const int numberOfChannels = 1;
        protected static Queue<byte[]> Packeges;
        protected List<byte[]> ForSend;
        protected List<byte[]> ForFifo;

        protected static int position = 0; //для прохождения по массиву файла
        protected static byte[] file = null;  //файл без заголовка, т.е. с 44 байта
        
        protected static Timer Timer; 
        private static int _losscount = 0;  //считает кол-во непришедших ответов  
        private static bool _timeOutFlag = false;  //флаг "время ожидания закончилось"
        private const int _waitTime = 140;  //время работы таймера
        private const int _infinite = -1;


        protected Sound(int myPort, string myIP, int fpsPort, string fpsIP, int iAmauntOfErrorBytes) 
            : base(myPort, myIP, fpsPort, fpsIP, iAmauntOfErrorBytes)
        { }


        public event EventHandler LostFPS;  //ФПС не отвечает

        //частота вещания помехи
        protected byte[] Converting(int frequency)
        {
            _frequencyKHz = new byte[3];
            int foo = frequency;
            byte lolo = (byte)(foo & 0xff);
            byte hilo = (byte)((foo >> 8) & 0xff);
            byte lohi = (byte)((foo >> 16) & 0xff);
            _frequencyKHz[2] = lohi;
            _frequencyKHz[1] = hilo;
            _frequencyKHz[0] = lolo;
            return _frequencyKHz;
        }

        //считает +1 потерю, если таймер закончился
        private void LossesCount(object state)
        {
            if (!youCanSendFlag)
            {
                _losscount += 1;
                _timeOutFlag = true;
            }

        }

        //отправляет кусочки аудио по приходу ответа от ФПС + выбрасывает event LostFPS при отсутсвии ответов
        protected void SendVoise()
        {
            var lossesCount = new TimerCallback(LossesCount);
            Timer = new Timer(lossesCount, _timeOutFlag, _infinite, _infinite); //для ожидания ответа

            int step = 0;  //нужен для подсчета отсутствующих ответов (отсчитывает 3 шага подряд, чтобы выкинуть ошибку)

            while (!voiceEndFlag)
            {
                //если не пришел 1 ответ, начать отсчет шагов (до 3)
                if (_losscount == 1)
                {
                    step = 1;
                }

                //отправка аудио (128 байт) по приходу ответа от ФПС или по окончанию таймера 
                if (youCanSendFlag || _timeOutFlag)
                {
                    Timer.Change(_infinite, _infinite);
                    _timeOutFlag = false;
                    switch (bOnlineORRecord)
                    {
                        case 0:
                            byte[] message = CopyPartOfRecord();
                            SendPartOfRecord(message);  //для файла   
                            break;
                        case 1:
                            if (Packeges.Count != 0)
                            {
                                SendPartOfRecord(Packeges.Dequeue());  //online
                            }
                            break;
                    }
                    youCanSendFlag = false;
                    Timer.Change(_waitTime, _infinite);
                    step++;
                }

                //проверка на 3 подряд не пришедших ответа
                if ((_losscount == 3) && (step == _losscount))
                {
                    OnWithoutParam(LostFPS);  //если не пришли три подряд ответа, кидаем исключение
                    _losscount = 0;
                    voiceEndFlag = true;
                }

                //обнуление счетчиков, если не 3 шага подряд не пришел ответ
                if (step == 3 || _losscount == 3)
                {
                    step = 0;
                    _losscount = 0;
                }
                Thread.Sleep(1);
            }
        }

        //возвращает фрагмент аудио длиной _minbyteaudio байт, начиная с позиции position
        private byte[] CopyPartOfRecord()
        {
            byte[] partOfRecord = new byte[_minbyteaudio];
            int difference = file.Length - 1 - position;
            if (difference < _minbyteaudio)
            {
                Array.Copy(file, position, partOfRecord, 0, difference);
                Array.Copy(Enumerable.Repeat((byte)0, _minbyteaudio - difference).ToArray(), 0, partOfRecord, difference - 1, _minbyteaudio - difference); //дополним нулями последнюю порцию аудио до 255
            }
            else
            {
                Array.Copy(file, position, partOfRecord, 0, partOfRecord.Length);
            }
            position += _minbyteaudio;
            if (position >= file.Length)
            {
                position -= file.Length;
            }
            return partOfRecord;
        } 

        //возвращает фрагмент online-речи длиной _minbyteaudio байт
        protected void StartOnlineStream(byte device)
        {
            try
            {
                numberOfDevice = device;
                byte[] partOfStream = new byte[_minbyteaudio];
                waveIn = new WaveInEvent();  
                waveIn.DeviceNumber = numberOfDevice;
                waveIn.BufferMilliseconds = _bufferSizeMs; 
                waveIn.WaveFormat = new WaveFormat(sampleRate, bitsInSample, numberOfChannels); //первое - частота дискретизации, 8 - кол-во бит в сэмпле, 1 - кол-во каналов
                waveIn.DataAvailable += new EventHandler<WaveInEventArgs>(waveIn_DataAvailable); 
                waveIn.RecordingStopped += new EventHandler<StoppedEventArgs>(waveIn_RecordingStopped); 
                waveIn.StartRecording(); 
            }
            catch (SystemException e)
            {
                //кидать событие?
            }

        }

        //начало обработки данных, пришедших с аудиокарты
        private void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            byte[] ForBuffer = new byte[e.BytesRecorded]; //порция данных (1024 байт, если размер буфера 128мс)
            Array.Copy(e.Buffer, 0, ForBuffer, 0, e.BytesRecorded); //копирование данных с аудио карты из буфера в эту порцию
            ToFifo(ForBuffer, e.BytesRecorded);
        }

        //Разбиение порции данных с аудиокарты на массивы по 128 байт
        private void ToFifo(byte[] ForBuffer, int partOFByte)
        {
            byte[] partFifo = new byte[_minbyteaudio];
            for (int i = 0; i < ForBuffer.Length; i += _minbyteaudio)
            {
                int difference = ForBuffer.Length - i;
                if (difference < _minbyteaudio)
                {
                    break;
                }
                else
                {
                    Array.Copy(ForBuffer, i, partFifo, 0, _minbyteaudio);
                    Packeges.Enqueue(partFifo.Clone() as byte[]);
                }
            }
        }

        //по окончанию чтения с карты закрыть поток
        private void waveIn_RecordingStopped(object sender, EventArgs e)
        {
            waveIn.Dispose();
            waveIn = null;
        }


       
        // Вызывается для отправки записи
        protected void StartSendVoice()
        {
            if (soundThread != null)
            {
                soundThread.Abort();
            }
            if (!soundONFlag)
            {
                soundONFlag = true;
                voiceEndFlag = false;
                if (bOnlineORRecord == 1)
                {
                    Packeges = new Queue<byte[]>();
                    ForSend = new List<byte[]>();
                    ForFifo = new List<byte[]>();

                    
                        StartOnlineStream(numberOfDevice);
                    
                }

                if (soundThread == null)
                {
                    Thread soundThread = new Thread(new ThreadStart(SendVoise));
                    soundThread.IsBackground = true;
                    soundThread.Start();
                }
            }
        }

        //останавливает аудиопомеху 
        protected void PauseVoice()
        {
            voiceEndFlag = true;
            if (waveIn != null)
            {
                waveIn.StopRecording();
            }
            if (Timer != null)
            {
                Timer.Dispose();
            }
            if (soundThread != null)
            {
                soundThread.Abort();
            }
            soundONFlag = false;

        }

        // Запрос на переход в режим речеподобной помехи или его отключение
        protected bool SendConfirmation(byte mode, int frequency, byte deviation)
        {
            byte[] freq = Converting(frequency);
            return CommanPartForCodograms(10, (byte)CodeogramCiphers.CONFIRM_SLI,
                delegate (ref byte[] ForSend)
                {
                    ForSend[5] = freq[0];
                    ForSend[6] = freq[1];
                    ForSend[7] = freq[2];
                    ForSend[8] = deviation;
                    ForSend[9] = mode;
                },
            null);
        }

        // Данные речеподобной помехи
        private bool SendPartOfRecord(byte[] recordForSending)
        {
            return CommanPartForCodograms(5 + _minbyteaudio, (byte)CodeogramCiphers.SEND_SLI,
                delegate (ref byte[] ForSend)
                {
                    Array.Copy(recordForSending, 0, ForSend, 5, _minbyteaudio);
                },
                recordForSending);
        }
    }
}
