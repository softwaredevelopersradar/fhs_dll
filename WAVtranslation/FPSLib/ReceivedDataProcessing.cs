﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace FPSLib
{
    public class ReceivedDataProcessing : UDP
    {
        private Thread readThread;  // поток приема данных
        private int _amauntOfErrorBytes;  //количество байт на ошибки в ответе от ФПС
        private byte[] _inform;
        private byte[] _codeError;   
        
        //для счетчика кодограм
        protected byte countSendCmd; 
        protected byte countReceiveCmd;
        protected byte countChiperSendCmd;


        protected ReceivedDataProcessing(int myPort, string myIP, int fpsPort, string fpsIP, int amauntOfErrorBytes) 
            : base (myPort, myIP, fpsPort, fpsIP)
        {
            _amauntOfErrorBytes = amauntOfErrorBytes;
            countSendCmd = countReceiveCmd = countChiperSendCmd = 0;
            readThread = new Thread(new ThreadStart(ReceiveData));
            readThread.IsBackground = true;
            readThread.Start();
        }


        // хэштаблица для быстрого определения наличия кодограмм
        protected HashSet<byte> CmdCodes = new HashSet<byte>(new byte[15] { 1, 2, 5, 6, 7, 8, 9, 10, 12, 15, 16, 17, 18, 13, 19 });
        protected enum CodeogramCiphers : byte // перечисление для шифров кодограм
        {
            REQUEST_STATE = 1,
            TYPE_LOAD,
            DURAT_PARAM_FWS = 5,
            REQUEST_VOLTAGE,
            REQUEST_POWER,
            REQUEST_CURRENT,
            REQUEST_TEMP,
            RADIAT_OFF,
            STOP_FHSS = 12,
            REQUEST_FPS_STATE,
            RESET = 15,
            CONFIRM_SLI,  
            SEND_SLI,  
            PARAM_FHSS_MANY,  
            SET_POWER
        }


        public event EventHandler LostAnyCmd;//Утеряна кодограмма
        public event EventHandler<byte[]> ReceiveByte;  
        
        public event EventHandler<OneValEventArgs> UpdateState;
        protected void OnUpdateSmth(EventHandler<OneValEventArgs> some_ev, byte[] inform, byte[] errorBytes)
        {
            var tErrorDataStructure = new ErrorDataStructure(errorBytes);
            var e = new OneValEventArgs(tErrorDataStructure, inform);
            some_ev?.Invoke(this, e);
        }

        public event EventHandler<ThreeValEventArgs> UpdatePower;
        protected void OnUpdateSmth(EventHandler<ThreeValEventArgs> some_ev, byte[] inform, byte[] errorBytes)
        {
            var tErrorDataStructure = new ErrorDataStructure(errorBytes);
            var power = new List<PowerParam>();
            //for (int i = 0; i < inform.Length / 3; i++)
            //{
            //    power.Add(new PowerParam(inform, i * 3));
            //}
            for (int i = 0; i < inform.Length; i++)
            {
                power.Add(new PowerParam(inform[i], i));
            }
            var e = new ThreeValEventArgs(tErrorDataStructure, power);
            some_ev?.Invoke(this, e);
        }

        public event EventHandler<TwoValEventArgs> UpdateVoltage;
        protected void OnUpdateSmth(EventHandler<TwoValEventArgs> some_ev, byte[] inform, byte[] errorBytes)
        {
            var tErrorDataStructure = new ErrorDataStructure(errorBytes);
            var voltage = new List<VoltageParam>();
            //for (int i = 0; i < inform.Length / 3; i++)
            //{
            //    voltage.Add(new VoltageParam(inform, i * 3));
            //}

            for (int i = 0; i < inform.Length / 2; i++)
            {
                voltage.Add(new VoltageParam(inform, i, i * 2));
            }
            TwoValEventArgs e = new TwoValEventArgs(tErrorDataStructure, voltage);
            some_ev?.Invoke(this, e);
        }

        public event EventHandler<FiveValEventArgs> UpdateCurrent;
        public event EventHandler<FiveValEventArgs> UpdateTemp;
        protected void OnUpdateSmth(EventHandler<FiveValEventArgs> some_ev, byte[] inform, byte[] errorBytes)
        {
            var tErrorDataStructure = new ErrorDataStructure(errorBytes);
            var fiveParams = new List<FiveParam>();
            //for (int i = 0; i < inform.Length / 5; i++)
            //{
            //    fiveParams.Add(new FiveParam(inform, i * 5));
            //}

            for (int i = 0; i < inform.Length / 4; i++)
            {
                fiveParams.Add(new FiveParam(inform, i, i * 4));
            }
            var e = new FiveValEventArgs(tErrorDataStructure, fiveParams);
            some_ev?.Invoke(this, e);
        }

        public event EventHandler<RecieveCmdEventArgs> ReceiveCmd;// получена команда
        protected virtual void OnReceiveCmd(EventHandler<RecieveCmdEventArgs> some_ev, byte bCode, byte[] error)
        {
            var tErrorDataStructure = new ErrorDataStructure(error);
            var e = new RecieveCmdEventArgs(bCode, tErrorDataStructure);
            some_ev?.Invoke(this, e);
        }

        public event EventHandler<StateEventArgs> RecivedStateFPS;// получена инфа о состоянии ФПС
        protected virtual void OnReceiveStateFPS(EventHandler<StateEventArgs> some_ev, byte[] error, byte[] bInform)
        {
            var tErrorDataStructure = new ErrorDataStructure(error);
            var e = new StateEventArgs(tErrorDataStructure, bInform[0], bInform[1]);
            some_ev?.Invoke(this, e);
        }


        //Проверка пришедших данных на наличие кодограммы
        private void ReceiveData()
        {
            while (true)
            {
                if (dataReceivedFlag)
                {

                    OnByteArray(ReceiveByte, dataRead);
                    if (dataRead.Length >= 5)
                    {
                        byte bCode = dataRead[2];  // шифр кодограммы
                        if (CmdCodes.Contains(bCode)) 
                        {
                            FillingForTCodeErrorInform(bCode, dataRead);
                            if (bCode!= (byte)CodeogramCiphers.SEND_SLI&&_inform!=null)
                            {
                                CheckData(bCode);
                            }
                        }
                    }
                    dataReceivedFlag = false;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }

        //проверка обновлений параметров литеры
        private void CheckData(byte bCode)
        {
            switch (bCode)
            {
                case (byte)CodeogramCiphers.REQUEST_STATE:
                    OnUpdateSmth(UpdateState,   _inform, _codeError);
                    break;
                case (byte)CodeogramCiphers.REQUEST_VOLTAGE:
                    OnUpdateSmth(UpdateVoltage, _inform, _codeError);
                    break;
                case (byte)CodeogramCiphers.REQUEST_POWER:
                    OnUpdateSmth(UpdatePower, _inform, _codeError);
                    break;
                case (byte)CodeogramCiphers.REQUEST_CURRENT:
                    OnUpdateSmth(UpdateCurrent, _inform, _codeError);
                    break;
                case (byte)CodeogramCiphers.REQUEST_TEMP:
                    OnUpdateSmth(UpdateTemp, _inform, _codeError);
                    break;
                case (byte)CodeogramCiphers.REQUEST_FPS_STATE:
                    OnReceiveStateFPS(RecivedStateFPS, _inform, _codeError);
                    break;
                default:
                    break;
            }
        }

        //чтение ошибки и доп.инфы в кодограмме от ФПС 
        private void FillingForTCodeErrorInform(byte NumberOfCMD, byte[] data)
        {
            _inform = (data.Length > (5 + _amauntOfErrorBytes) ? new byte[data.Length - (5 + _amauntOfErrorBytes)] : null);
            if (_amauntOfErrorBytes == 1)  //так сделано, потому что для исходного протокола есть некоторые различия в кодах ошибок для разных кодограмм 
            { _codeError = new byte[2] { data[2], data[5] }; }
            //{ _codeError = new byte[1] { data[5] }; }
            else if (_amauntOfErrorBytes == 3)
            {
                _codeError = new byte[_amauntOfErrorBytes]; // код ошибки
                try
                { Array.Copy(data, 5, _codeError, 0, _amauntOfErrorBytes); }
                catch {  }
                OnReceiveCmd(ReceiveCmd, NumberOfCMD, _codeError); // передать параметры в событие ReceiveCMD
            }
            if (data.Length > 5 + _amauntOfErrorBytes)
                Array.Copy(data, 5 + _amauntOfErrorBytes, _inform, 0, data.Length - (5 + _amauntOfErrorBytes));
            CatchesLosses();
        }

        //Проверка кол-ва пакетов на наличие потерь
        private void CatchesLosses()
        {
            countReceiveCmd = Convert.ToByte((countReceiveCmd == 255 ? 0 : countReceiveCmd + 1));
            if (countReceiveCmd != countSendCmd) 
            {
                OnWithoutParam(LostAnyCmd); 
                countReceiveCmd = 0;
                countSendCmd = 0;
            }
        }

    }


}
