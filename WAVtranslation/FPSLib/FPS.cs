﻿using System;
using System.Collections.Generic;
using ModelsTablesDBLib;
using NAudio.Wave;

namespace FPSLib
{
    public class FPS : Sound, IFPSLib
    {
        
        private int OffsetFreqFHSS = 10000; //для ППРЧ отнимать от min частоты


        public FPS(int myPort, string myIP, int fpsPort, string fpsIP, int iAmauntOfErrorBytes) 
            : base(myPort, myIP, fpsPort, fpsIP, iAmauntOfErrorBytes)
        { }


        // Выключить излучение
        public bool SetRadiatOff()
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.RADIAT_OFF, null, null);
        }

        // Состояние литер
        public bool GetStateAP(List<byte> letter)
        {
            return CommanPartForCodograms(5 + letter.Count, (byte)CodeogramCiphers.REQUEST_STATE,
                delegate (ref byte[] ForSend)
                {
                    for (int i = 0; i < letter.Count; i++)
                    {
                        ForSend[5+i] = letter[i];
                    }
                },
                letter);
        }

        // Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        public bool SetParamFWS(List<TableSuppressFWS> tableSuppressFWS, int duration)
        {try
            {
                return CommanPartForCodograms(8 + tableSuppressFWS.Count * 7, (byte)CodeogramCiphers.DURAT_PARAM_FWS,
                    delegate (ref byte[] ForSend)
                    {
                        Array.Copy(BitConverter.GetBytes(duration * 10), 0, ForSend, 5, 3);  //возможно удалить умножение
                        for (int i = 0; i < tableSuppressFWS.Count; i++)
                        {
                            Array.Copy(BitConverter.GetBytes((int)(tableSuppressFWS[i].FreqKHz.Value)), 0, ForSend, i * 7 + 8, 3);
                            ForSend[i * 7 + 11] = tableSuppressFWS[i].InterferenceParam.Modulation;
                            ForSend[i * 7 + 12] = tableSuppressFWS[i].InterferenceParam.Deviation;
                            ForSend[i * 7 + 13] = tableSuppressFWS[i].InterferenceParam.Manipulation;
                            ForSend[i * 7 + 14] = tableSuppressFWS[i].InterferenceParam.Duration;
                        }
                    },
                    tableSuppressFWS);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // Установить параметры РП для ИРИ ППРЧ (несколько сетей)
        public bool SetParamFHSS(List<TableSuppressFHSS> tableSuppressFHSS, int Duration, byte CodeFFT)
        {
            
            return CommanPartForCodograms(9 + tableSuppressFHSS.Count * 6, (byte)CodeogramCiphers.PARAM_FHSS_MANY,
                delegate (ref byte[] ForSend)
                {
                    Array.Copy(BitConverter.GetBytes(Duration * 10), 0, ForSend, 5, 3);// возможно удалить умножение
                    ForSend[8] = CodeFFT;
                    for (int i = 0; i < tableSuppressFHSS.Count; i++)
                    {
                        Array.Copy(BitConverter.GetBytes((int)tableSuppressFHSS[i].FreqMinKHz - OffsetFreqFHSS), 0, ForSend, 6 * i + 9, 3);
                        ForSend[6 * i + 12] = tableSuppressFHSS[i].InterferenceParam.Modulation;
                        ForSend[6 * i + 13] = tableSuppressFHSS[i].InterferenceParam.Deviation;
                        ForSend[6 * i + 14] = tableSuppressFHSS[i].InterferenceParam.Manipulation;
                    }
                },
                tableSuppressFHSS);
        }

        //Запрос "Напряжение усилителя литеры"
        public bool GetVoltageAP(List<byte> letter)
        {
            return CommanPartForCodograms(5 + letter.Count, (byte)CodeogramCiphers.REQUEST_VOLTAGE,
                delegate (ref byte[] ForSend)
                {
                    for (int i = 0; i < letter.Count; i++)
                    {
                        ForSend[5 + i] = letter[i];
                    }
                },
                letter);
        }

        // Запрос "Выходная мощность усилителя литеры"
        public bool GetPowerAP(List<byte> letter)
        {
            return CommanPartForCodograms(5 + letter.Count, (byte)CodeogramCiphers.REQUEST_POWER,
                delegate (ref byte[] ForSend)
                {
                    for (int i = 0; i < letter.Count; i++)
                    {
                        ForSend[5 + i] = letter[i];
                    }
                },
                letter);
        }

        //Запрос "ток потребления усилителителем мощности литеры"
        public bool GetCurrentAP(List<byte> letter)
        {
            return CommanPartForCodograms(5 + letter.Count, (byte)CodeogramCiphers.REQUEST_CURRENT,
                delegate (ref byte[] ForSend)
                {
                    for (int i = 0; i < letter.Count; i++)
                    {
                        ForSend[5 + i] = letter[i];
                    }
                },
                letter);
        }

        //Запрос "температуры усилительных модулей литеры"
        public bool GetTempAP(List<byte> letter)
        {
            return CommanPartForCodograms(5 + letter.Count, (byte)CodeogramCiphers.REQUEST_TEMP,
                delegate (ref byte[] ForSend)
                {
                    for (int i = 0; i < letter.Count; i++)
                    {
                        ForSend[5 + i] = letter[i];
                    }
                },
                letter);
        }

        // Запрос "Установить тип нагрузки"
        public bool SetTypeLoad(byte letter, byte type)  //Может тут тоже список литер? 
        {
            return CommanPartForCodograms(7, (byte)CodeogramCiphers.TYPE_LOAD,
                delegate (ref byte[] ForSend)
                {
                    ForSend[5] = letter;
                    ForSend[6] = type;
                },
                letter); 
        }

        //Сброс аварии
        public bool ResetError()
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.RESET, null, null);
        }

        // Выход из режима РП-ППРЧ
        public bool StopFHSS()
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.STOP_FHSS, null, null);
        }

        //Запрос состояния ФПС
        public bool StateFHS()
        {
            return CommanPartForCodograms(5, (byte)CodeogramCiphers.REQUEST_FPS_STATE, null, null);
        }

        // Проверка наличия файла по заданному пути
        public bool SetVoice(string filepath)
        {
            if (System.IO.File.Exists(filepath))
            {
                byte[] fullfile = System.IO.File.ReadAllBytes(filepath);
                file = new byte[fullfile.Length - 44];
                Array.Copy(fullfile, 44, file, 0, fullfile.Length - 44);
                position = 0;
                bOnlineORRecord = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        // Проверка наличия устройства с заданным номером
        public bool SetVoice(byte numberOfDevice)
        {
            try
            {
                var tr = WaveIn.GetCapabilities(numberOfDevice);
                this.numberOfDevice = numberOfDevice;
                bOnlineORRecord = 1;
                return true;
            }
            catch
            {
                return false;
            } 
        }

        //Если SetVoice ответил true, передать частоту вещания помехи и режим (1 – включить, 0 – выключить помеху)
        public void RegimeVoice( byte bMode,int frequencyKHz, byte deviation)
        {
            SendConfirmation(bMode, frequencyKHz, deviation);  //проверить будет ли меняться частота вещания при нажатии тангенты
            if (bMode == 1)
            {
                StartSendVoice();
            }
            if (bMode == 0)
            {
                PauseVoice();
            }
        }


        //Установить мощность усилителя
        public bool SetPowerAP(List<SetPowerLetter> setPowerLetter)
        {
            return CommanPartForCodograms(5 + setPowerLetter.Count * 2, (byte)CodeogramCiphers.SET_POWER,
                delegate (ref byte[] ForSend)
                {
                    for (int i = 0; i < setPowerLetter.Count; i++)
                    {
                        ForSend[i * 2 + 5] = setPowerLetter[i].Letter;
                        ForSend[i * 2 + 6] = setPowerLetter[i].Power;
                    }
                },
                setPowerLetter);
        }
    }
}
