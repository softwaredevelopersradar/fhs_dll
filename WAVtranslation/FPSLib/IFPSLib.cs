﻿using System;
using System.Collections.Generic;
using ModelsTablesDBLib;

namespace FPSLib
{
    interface IFPSLib
    {
        bool SetRadiatOff(); // (шифр 10) выключить излучение
        bool SetParamFWS(List<TableSuppressFWS> tableSuppressFWS, int Duration);   // (шифр 5) Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности
        bool SetParamFHSS(List<TableSuppressFHSS> tableSuppressFHSS, int Duration, byte CodeFFT); // (шифр 18) запрос "Установить параметры РП для ИРИ ППРЧ" (несколько сетей)
        bool SetTypeLoad(byte bLetter, byte bType);  // (шифр 2) Запрос "Установить тип нагрузки"
        bool StopFHSS(); // (шифр 12) Выход из режима РП-ППРЧ
        bool ResetError(); // (шифр 15) Сброс аварии   
        bool StateFHS(); // (шифр 13) запрос состояния ФПС   

        bool GetStateAP(List<byte> letter); // (шифр 1) Запрос "состояние одной литеры"
        bool GetVoltageAP(List<byte> letter);   //(шифр 6) Запрос "напряжение усилителя литеры"
        bool GetCurrentAP(List<byte> lletter);  //(шифр 8) Запрос "ток потребления усилителителем мощности литеры"
        bool GetTempAP(List<byte> letter);   //(шифр 9) Запрос "температуры усилительных модулей литеры"
        bool GetPowerAP(List<byte> letter); //(шифр 7) Запрос "выходная мощность усилителя литеры"

        bool SetVoice(string filepath);   
        bool SetVoice(byte numberOfDevice); 
        void RegimeVoice(byte bMode,int frequency, byte deviation ); 

        bool SetPowerAP(List<SetPowerLetter> setPowerLetter);  //(шифр 19) Мощность усилителя  (установить)

        void Connect();
        void Disconnect();
        

        event EventHandler ConnectNet; // событие подключния
        event EventHandler ConnectFailed;
        event EventHandler DisconnectNet; // событие отключения
        event EventHandler<byte[]> ReceiveByte; // получен массив байт
        event EventHandler<byte[]> SendByte;// отпрвлен массив байт


        event EventHandler<OneValEventArgs> UpdateState;
        event EventHandler<ThreeValEventArgs> UpdatePower;
        event EventHandler<TwoValEventArgs> UpdateVoltage;
        event EventHandler<FiveValEventArgs> UpdateCurrent;
        event EventHandler<FiveValEventArgs> UpdateTemp;
        event EventHandler<StateEventArgs> RecivedStateFPS;

        event EventHandler LostAnyCmd; //Утеряна кодограмма

        event EventHandler<RecieveCmdEventArgs> ReceiveCmd;// событие получения команды
        event EventHandler<byte> SendCmd; // отправлена кодограмма

        event EventHandler LostFPS;  //ФПС не отвечает (на речеподобную помеху)

    }
}
